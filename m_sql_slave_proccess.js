function m_sql_slave_proccess(sql, callback){      
    process.nextTick(function() {
		pool = mysql.createPool({
			multipleStatements: true,
			host: "slave",
			user: "api",
			password: "(#&&48iuiUH(&+",
			database: "digivla",
			port: "3306",
			});
			pool.getConnection(function(err, connection) {
				if (err) {
					connection.release();
					console.log(' Error getting mysql_pool connection: ' + err);
					throw err;
				}
				connection.query(sql, function(err,rows){
					connection.release();
					if(err){
					return callback(err);	
					}else{
					return callback(rows);
					}
				});
			});
	});     
}
exports.m_sql_slave_proccess = m_sql_slave_proccess;
