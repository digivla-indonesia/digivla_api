function m_sent_data(fields, path, method, callback){      
    process.nextTick(function() {
		var postData	= JSON.stringify(fields);
		var options = {
				  // host: '10.10.10.9',
				  // host: 'elastic.antara-insight.id',
				  host: '172.16.0.3',
				  path: "/"+path,
				  method: method,
				  headers: {
					'Content-Type': 'application/json'
					}
				};
//		console.log("ASRI----");
//		console.log(options);
		var req = http.request(options, function(res) {
				var data = '';
				//res.setEncoding('utf8');
				res.on('data', function (chunk) {
				data += chunk;
				//return callback(result);
				});
				res.on('end', function() {
				input	= data.replace("c:out escapeXml='false' value='", "");
				//obj = JSON.parse(data);
				return callback(input);
				});
		});
//		console.log(req);
		req.on('error', function(e) {
		console.log('problem with request: ' + e.message);
		return callback(e.message);
		});
		// write data to request body
		req.write(postData);
		req.end();
	});     
}
exports.m_sent_data = m_sent_data;
