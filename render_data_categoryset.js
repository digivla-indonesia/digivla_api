function render_data_categoryset(rows, callback){      
    process.nextTick(function() {
		if(rows.hits.total > 0){
		 var arrayForChart = [];
			for(i=0; i < rows.hits.hits.length; i++){
				var item = {
				  "match": {
					"category_id": {
					  "query": rows.hits.hits[i]._source.category_id,
					  "operator": "and"
					}
				  }
				}
				arrayForChart.push(item);
			}
		return callback({success: true, data: arrayForChart});
		}else{
		return callback({success: false, data:[]});	
		}
	});     
}
exports.render_data_categoryset = render_data_categoryset;