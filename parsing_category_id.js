function parsing_category_id(category_id, callback){      
    process.nextTick(function() {
		if(category_id.length > 0){
			 var arrayForcat = [];
			for(i=0; i < category_id.length; i++){
				var item = {
				  "match": {
					"category_id": {
					  "query": category_id[i],
					  "operator": "and"
					}
				  }
				}
				arrayForcat.push(item);
			}
		}
	return callback(arrayForcat);
	});     
}
exports.parsing_category_id = parsing_category_id;