function render_data_mediasetcho(rows, callback){      
    process.nextTick(function() {
		if(rows.hits.total > 0){
		 var arrayForChart = [];
			for(i=0; i < rows.hits.hits.length; i++){
			arrayForChart.push(rows.hits.hits[i].fields.user_media_id[0]);
			}
		return callback({success: true, data: arrayForChart});
		}else{
		return callback({success: false, data:[]});	
		}
	});     
}
exports.render_data_mediasetcho = render_data_mediasetcho;