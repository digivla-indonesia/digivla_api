function render_article_id(rows, callback){      
    process.nextTick(function() {
		var arrayForChart = [];
			for(i=0; i < rows.length; i++){
				var item = {
				  "match": {
					"article_id": {
					  "query": rows[i],
					  "operator": "and"
					}
				  }
				}
				arrayForChart.push(item);
			}
		return callback(arrayForChart);
	});     
}
exports.render_article_id = render_article_id;