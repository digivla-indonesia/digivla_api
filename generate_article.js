function generate_article(full, rows, callback){      
    process.nextTick(function() {
		var data_rows = [];
		if(rows.length > 0){
			for(i=0; i < rows.length; i++){
			var content 	= !full ? truncate(rows[i]._source.content, 100) : rows[i]._source.content;
			data_rows[i] 	= {'article_id': rows[i]._source.article_id, 'title': rows[i]._source.title, 'media_id': rows[i]._source.media_id, 'datee': rows[i]._source.datee, 'content': content, 'mmcol': rows[i]._source.mmcol, 'circulation': rows[i]._source.circulation, 'page': rows[i]._source.page, 'file_pdf': rows[i]._source.file_pdf, 'columne': rows[i]._source.columne, 'size_jpeg': rows[i]._source.size_jpeg, 'journalist': rows[i]._source.journalist, 'rate_bw': rows[i]._source.rate_bw, 'rate_fc': rows[i]._source.rate_fc, 'is_chart': rows[i]._source.is_chart, 'is_table': rows[i]._source.is_table, 'is_colour': rows[i]._source.is_colour, 'data_input_date': rows[i]._source.data_input_date};	
			}
		}
	return callback(data_rows);
	});     
}
exports.generate_article = generate_article;